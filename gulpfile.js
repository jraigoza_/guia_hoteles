'use-strict'

const browserSync = require('browser-sync');
const gulp = require('gulp');
const sass = require('gulp-sass');
const del = require('del');
const imagemin = require('gulp-imagemin');
const uglify = require('gulp-uglify');
const usemin = require('gulp-usemin');
const rev = require('gulp-rev');
const cleanCss = require('gulp-clean-css');
const flatmap = require('gulp-flatmap');
const htmlmin = require('gulp-htmlmin');

gulp.task('sass', function () {
    return gulp.src('./css/*.scss')
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest('./css'));
});

gulp.task('sass:watch', function () {
    return gulp.watch('./css/*.scss', ['sass']);
});

gulp.task('browser-sync', function () {
    var files = ['./*.html', './css/*.css', './images/*.{png, gif, jpg, jpeg}', './js/*.js']
    browserSync.init(files, {
        server: {
            baseDir: './'
        }
    });
});

gulp.task('default', gulp.series('browser-sync', function () {
    return gulp.start('sass:watch');
}));


gulp.task('clean', function () {
    return del(['dist']);
});

gulp.task('imagemin', function () {
    return gulp.src('./images/*.{png, jpg, gif, jpeg}')
    .pipe(imagemin({ optimizationLevel: 3, progressive: true, interlaced: true }))
    .pipe(gulp.dest('dist/images'));
});

gulp.task('usemin', function () {
    return gulp.src('./*.html')
    .pipe(flatmap(function (stream, file) {
        return stream
        .pipe(usemin({
            css: [rev()],
            html: [function () { return htmlmin({ collapseWhitespace: true }) }],
            js: [uglify(), rev()],
            inlinejs: [uglify()],
            inlinecss: [cleanCss(), 'concat']
        }));
    }))
    .pipe(gulp.dest('dist/'));
});


gulp.task('copyfonts', function() {
    return gulp.src('./node_modules/open-iconic/font/fonts/*.{ttf,woff,eof,svg,eot,otf}*')
    .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('build', gulp.series('clean','copyfonts','imagemin','usemin'));
