$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle=popover]").popover();
    // En caso que se quiera modificar el intervalo con el que rotan las imágenes
    $('.carousel').carousel({
        interval: 4000
    });


    $('#contacto').on('show.bs.modal', function (e) {
        console.log("modal se está mostrando");
        $('#contactoBtn').removeClass('btn-outline-success');
        $('#contactoBtn').addClass('btn-primary');
        $('#contactoBtn').prop('disabled', true);
    });

    $('#contacto').on('shown.bs.modal', function (e) {
        console.log("modal se mostró");
    });
    $('#contacto').on('hide.bs.modal', function (e) {
        console.log("modal se está ocultando");
    });
    $('#contacto').on('hidden.bs.modal', function (e) {
        console.log("modal se ocultó");
        $('#contactoBtn').addClass('btn-outline-success');
        $('#contactoBtn').css("background-color", "transparent");
        $('#contactoBtn').prop('disabled', false);
    });
});